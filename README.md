# Moodtracker

## Project description
This project is a aimed to be a pretty simple mood tracker, where a user will be able to put a color that represents his mood, on a particular day.

The main goal is to have a colorful "heatmap" at the end of the year (kind of Github participation heatmap, but with moods/more colors).

Some new features should be developed, more or less soon.   

## When Moodtracker came in my developer journey - Jan 2019  
I started working on this project right after in parallel with Lazyker.
 
I'm currently still working on this project, when I find some time to do so (but not as often as I work on Lazyker). 

## What I learned with Moodtracker
Not so much for now, as I just initiated the project, but I hope I will learn a few more things with this one.  

### Moodtracker : Currently not hosted anywhere (the project)