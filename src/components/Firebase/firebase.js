import database from '@firebase/database';
import '@firebase/auth';
import firebase from '@firebase/app';

const config = {
    apiKey: "AIzaSyAvEpZgRy56GlgjQ8DRijSjaf6m6w-ntmk",
    authDomain: "moodtracker-97e68.firebaseapp.com",
    databaseURL: "https://moodtracker-97e68.firebaseio.com",
    projectId: "moodtracker-97e68",
    storageBucket: "moodtracker-97e68.appspot.com",
    messagingSenderId: "617796196028"
};

// if (!firebase.apps.length) {
//     firebase.initializeApp(config);
// }

// export default {
//     firebase
// };

class Firebase {
    constructor() {

        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }

        // firebase.initializeApp(config);
        console.log(firebase);

        this.auth = firebase.auth();
        this.database = firebase.database();

        // this.doSignInWithEmailAndPassword = firebase.auth().signInWithEmailAndPassword
    }
}

export default new Firebase();