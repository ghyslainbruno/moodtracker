import HeatMap from 'react-heatmap-grid'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {Component} from "react";
import React from "react";
import Paper from '@material-ui/core/Paper';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import FaceIcon from '@material-ui/icons/Face';
import DoneIcon from '@material-ui/icons/Done';
import Touch from '@material-ui/icons/TouchApp';
import pink from '@material-ui/core/colors/pink';
import orange from '@material-ui/core/colors/orange';
import green from '@material-ui/core/colors/green';
import blue from '@material-ui/core/colors/blue';
import grey from '@material-ui/core/colors/grey';
import purple from '@material-ui/core/colors/purple';
import red from '@material-ui/core/colors/red';
import brown from '@material-ui/core/colors/brown';
import Avatar from '@material-ui/core/Avatar';


const styles = {
    pinkAvatar: {
        backgroundColor: pink[500],
    },
    orangeAvatar: {
        backgroundColor: orange[500],
    },
    greenAvatar: {
        backgroundColor: green[500],
    },
    blueAvatar: {
        backgroundColor: blue[500],
    },
    greyAvatar: {
        backgroundColor: grey[500],
    },
    purpleAvatar: {
        backgroundColor: purple[500],
    },
    redAvatar: {
        backgroundColor: red[500],
    },
    brownAvatar: {
        backgroundColor: brown[500],
    },
};


const moods = [];
moods.push({name: 'Amazing', color: 'pink', code: '#e91e63'});
moods.push({name: 'Happy', color: 'orange', code: '#ff9800'});
moods.push({name: 'Normal', color: 'green', code: '#4caf50'});
moods.push({name: 'Depressed', color: 'blue', code: '#2196f3'});
moods.push({name: 'Sad', color: 'grey', code: '#9e9e9e'});
moods.push({name: 'Tiring', color: 'purple', code: '#9c27b0'});
moods.push({name: 'Angry', color: 'red', code: '#f44336'});
moods.push({name: 'Stressful', color: 'brown', code: '#795548'});

const xLabels = new Array(12).fill(0).map((_, i) => `${new Date(2018,i,1).toLocaleString('en-us', { month: 'short' }).charAt(0)}`);
const yLabels = new Array(31).fill(0).map((_, i) => `${i+1}`);
const data = new Array(yLabels.length)
    .fill(0)
    .map(() => new Array(xLabels.length).fill(0).map(() => '#e0e0e0'));


class MoodTracker extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: data,
            moodSelected: null,
            dateSelected: new Date(),
            open: false,
            selectedValue: moods[1],
        };
    }

    handleClickOpen = (month, day) => {

        this.setState({
            dateSelected: new Date(2018, month, day + 1),
            open: true,
            coordinatesDaySelected : {x: month, y: day},
            moodSelected: null
        });
    };

    validateNewMood = () => {

        const day = this.state.coordinatesDaySelected.y;
        const month = this.state.coordinatesDaySelected.x;

        // Finding the mood object
        const moodWanted = moods.filter(mood => {
            return mood === this.state.moodSelected
        });

        const data = this.state.data;
        data[day][month] = moodWanted[0].code;

        this.setState({
            data: data
        });

        this.handleClose()
    };

    handleClose = value => {
        this.setState({ selectedValue: value, open: false });
    };


    render() {

        const { classes } = this.props;

        return (
            <Paper className="moodTracker">

                <Dialog onClose={this.handleClose} aria-labelledby="simple-dialog-title" open={this.state.open} >
                    <DialogTitle id="simple-dialog-title">Select a mood</DialogTitle>

                    <DialogContent>
                        <DialogContentText>
                            {this.state.dateSelected.toDateString()}
                        </DialogContentText>

                        <div>
                            <List>
                                {moods.map(mood => (
                                    <ListItem button onClick={() => this.setState({moodSelected: mood})} key={mood.name}>
                                        <Chip
                                            avatar={
                                                <Avatar className={classes[mood.color + 'Avatar']}>
                                                    <DoneIcon style={{display: (mood === this.state.moodSelected ? 'inline' : 'none'), color: 'white'}}/>
                                                </Avatar>}
                                            label={mood.name}
                                            clickable
                                            variant="outlined"
                                        />
                                    </ListItem>
                                ))}
                            </List>
                        </div>
                    </DialogContent>

                    <DialogActions>
                        <Button color="primary" onClick={this.handleClose}>
                            Cancel
                        </Button>
                        <Button color="primary" autoFocus onClick={() => this.validateNewMood()}>
                            Ok
                        </Button>
                    </DialogActions>

                </Dialog>

                <HeatMap
                    xLabels={xLabels}
                    yLabels={yLabels}
                    xLabelsLocation={"top"}
                    data={this.state.data}
                    height={30}
                    xLabelWidth={60}
                    yLabelWidth={25}
                    squares={false}
                    onClick={(month, day) => this.handleClickOpen(month, day)}
                    cellStyle={(background, value, min, max, data, x, y) => {

                        return ({
                            background: `${value}`,
                            // background: `rgb(66, 86, 244, ${1 - (max - value) / (max - min)})`,
                            fontSize: "11px",
                        })
                    }}
                    // cellRender={value => value && `${'value'}%`}
                />
            </Paper>
        );
    }
}

MoodTracker.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MoodTracker);
