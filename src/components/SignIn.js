import React, { Component } from 'react';
import {Link, withRouter} from 'react-router-dom';
import Divider from '@material-ui/core/Divider';
import { SignUpLink } from './SignUp';
// import { auth } from '../firebase';
import firebase from './Firebase/firebase';
import * as routes from '../constants/routes';

// Trying the new material spec
import Button from '@material-ui/core/Button';

// import Button from '@material/react-button/dist';
import TextField from '@material-ui/core/TextField';

// import TextField, {HelperText, Input} from '@material/react-text-field';
import Paper from '@material-ui/core/Paper';
import CircularProgress from "@material-ui/core/CircularProgress";
import {GoogleProvider, Providers} from "./SignInProviders";

const auth = firebase.auth;

const SignInPage = ({ history }) =>
    <div>
        {/*<h1>SignIn</h1>*/}
        <SignInForm history={history} />
        {/*<SignUpLink />*/}
    </div>

const byPropKey = (propertyName, value) => () => ({
    [propertyName]: value,
});

const INITIAL_STATE = {
    email: '',
    password: '',
    signInLoading: false,
    error: null,
};

class SignInForm extends Component {
    constructor(props) {
        super(props);

        this.state = { ...INITIAL_STATE };
    }

    onSubmit = (event) => {

        this.setState({signInLoading: true});

        const {
            email,
            password,
        } = this.state;

        const {
            history,
        } = this.props;

        auth.signInWithEmailAndPassword(email, password)
            .then(() => {
                this.setState({signInLoading: false});
                this.setState({ ...INITIAL_STATE });
                history.push(routes.MOVIES);
            })
            .catch(error => {
                this.setState({signInLoading: false});
                this.setState(byPropKey('error', error));
            });

        event.preventDefault();
    };

    render() {
        const {
            email,
            password,
            error,
        } = this.state;

        const isInvalid =
            password === '' ||
            email === '';

        return (

            <Paper elevation={1} className="signInCard">

                <div style={{color: 'white'}}>
                    Sign In into Lazyker
                </div>

                <form onSubmit={this.onSubmit}>

                    <div>
                        <TextField
                            className="authField"
                            variant="outlined"
                            label='Email'
                            style={{width: '100%', marginBottom: '10px'}}
                            value={email}
                            onChange={event => this.setState(byPropKey('email', event.target.value))}
                        >
                            {/*<Input*/}
                                {/*value={email}*/}
                                {/*onChange={event => this.setState(byPropKey('email', event.target.value))}/>*/}
                        </TextField>
                    </div>

                    <div>
                        <TextField
                            className="authField"
                            variant="outlined"
                            label='Password'
                            style={{width: '100%'}}
                            value={password}
                            onChange={event => this.setState(byPropKey('password', event.target.value))}
                        >
                            {/*<Input*/}
                                {/*value={password}*/}
                                {/*onChange={event => this.setState(byPropKey('password', event.target.value))}/>*/}
                        </TextField>
                    </div>

                    <div style={{width: '100%', display: 'inline-block', textAlign: 'center'}}>
                        <Button
                            className="signInBtn"
                            outlined={false}
                            unelevated={true}
                            disabled={isInvalid || this.state.signInLoading}
                            type="submit">
                            {this.state.signInLoading ?
                                <b>Loading</b>
                                :
                                <b>Sign In</b>
                            }
                        </Button>

                        <SignUpLink />

                    </div>

                    <Divider/>

                    <Providers/>

                    { error && <p>{error.message}</p> }
                </form>
            </Paper>


        );
    }
}

const SignInLink = () =>
    <p>
        Already have an account?
        {' '}
        <Link style={{color: 'red'}} to={routes.SIGN_IN}>Sign In</Link>
    </p>;

export default withRouter(SignInPage);

export {
    SignInForm,
    SignInLink
};