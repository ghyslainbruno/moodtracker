import React, { Component } from 'react';
import MoodTracker from './components/moodtracker/MoodTracker';
import firebase from './components/Firebase/firebase';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from '@material-ui/core/Button';
import {SignUpForm} from "./components/SignUp";
import {SignInForm} from "./components/SignIn";


import './App.scss';

const theme = createMuiTheme({
    palette: {
        // type: 'dark', // Switching the dark mode on is a single property value change.
        // primary: red,
        // secondary: green,
    },
});

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            userLoading: true,
            authUser: null
        };
    }

    componentDidMount() {
        this.setState({userLoading: true});
        firebase.auth.onAuthStateChanged(authUser => {
            this.setState({userLoading: false});
            authUser
                ?
                this.setState({ authUser })
                :
                this.setState({ authUser: null });
        });
    }

  render() {
      return (
          <Router>
              <MuiThemeProvider theme={theme}>

                  {

                      this.state.userLoading ?

                          <div>
                              <div style={{width: '100%', marginTop: '50vh', textAlign: 'center'}}>
                                  <CircularProgress style={this.state.userLoading ? {display: 'inline-block'} : {display: 'none'}}/>
                              </div>
                          </div>

                          :

                          this.state.authUser ?
                              <div className="App">
                                  <h1>
                                      Mood Tracker
                                  </h1>
                                  <h2>Just remember: </h2>
                                  <h3>Even your worst day only lasts 24 hours</h3>

                                  <MoodTracker style={{width: '30%', margin: '0 auto'}}/>
                              </div>
                              :
                              <div>
                                  <Route exact path='/signup' render={() =><SignUpForm />}/>
                                  <Route path={/^(?!.*(signup|privacy_policy)).*$/} render={() =><SignInForm />}/>
                              </div>

                  }

              </MuiThemeProvider>
          </Router>
      );
  }
}

export default App;
